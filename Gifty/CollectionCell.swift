//
//  CollectionCell.swift
//  Gifty
//
//  Created by Halimjon Juraev on 6/29/16.
//  Copyright © 2016 Halimjon Juraev. All rights reserved.
//

import UIKit

let SHADOW_COLOR: CGFloat = 157.0 / 255


class CollectionCell: UICollectionViewCell {
    static var cache = NSCache()

    let session = NSURLSession.sharedSession()
    @IBOutlet weak var TrendingImage: UIImageView!
    @IBOutlet weak var MainImage: UIImageView!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 5.0
        layer.shadowColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).CGColor
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSizeMake(0.0, 2.0)
    }
    

    
    func configureCell(view: Gifs) {
        
        MainImage.image = nil
        TrendingImage.hidden = true
        if let currentCache = CollectionCell.cache.objectForKey(view.url) as? NSData{
            self.MainImage.image = UIImage.gifWithData(currentCache)
            
        } else {
            self.setup(view.url)

        }
        

        if view.trending_datetime.compare(view.import_datetime) == NSComparisonResult.OrderedDescending
        {
          TrendingImage.hidden = false
        
        } else {
            TrendingImage.hidden = true
        }
        
        TrendingImage.image = UIImage(named: "trend")
    }
    
    
    func setup(url: NSURL) {
        
        session.dataTaskWithURL(url) { data, response, error in
          dispatch_async(dispatch_get_main_queue()){
            if let gifData = data {
            CollectionCell.cache.setObject(gifData, forKey: url)
            self.MainImage.image = UIImage.gifWithData(gifData)
                
            }
            }
        }.resume()
        
    }
    
}
