//
//  FetchGif.swift
//  Gifty
//
//  Created by Halimjon Juraev on 6/29/16.
//  Copyright © 2016 Halimjon Juraev. All rights reserved.
//

import UIKit

class Gifs {
    let url: NSURL!
    let trending_datetime: NSDate!
    let import_datetime: NSDate!
    let rate: String!
    
    
    init(url: String, trending_datetime: NSDate, import_datetime: NSDate, rate: String) {
        
        self.url = NSURL(string: url)
        self.trending_datetime = trending_datetime
        self.import_datetime = import_datetime
        self.rate = rate
        
    }
}


class Network: NSObject {
    
    var GIFS = [Gifs]()
    
    let session = NSURLSession.sharedSession()
    
    func fetch(method: String, limit: String, rating: String, format: String, completion: (result: Bool, Gifs: [Gifs]?) -> Void) {
        GIFS.removeAll()
        if let url = NSURL(string: "http://api.giphy.com/v1/gifs/\(method)api_key=dc6zaTOxFJmzC&limit=\(limit)&fmt=\(format)") {
            print(url)
        session.dataTaskWithURL(url) { data, response, error in
            if error == nil {
            
                do {
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! [String: AnyObject]
                    if let info = json["data"] as? [AnyObject] {
                        for gif in info {
                            if let images = gif["images"] as? [String: AnyObject] {
                                if let url = images["fixed_height"]?["url"]!! as? String {
                                    
                                    if let trending = gif["trending_datetime"] as? String {
                                        if let upload = gif["import_datetime"] as? String {
                                            if let rate = gif["rating"] as? String {
                                                let formatter = NSDateFormatter()
                                                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                                if let trending_datetime = formatter.dateFromString(trending) {
                                                    if let import_datetime = formatter.dateFromString(upload) {
                                                        self.GIFS.append(Gifs(url: url, trending_datetime: trending_datetime, import_datetime: import_datetime, rate: rate))
                                                    
                                                    }
                                                }

                                            
                                            }
                                        }
                                    }



                                    
                                }
                            }
                        }
                    }
                    completion(result: true, Gifs: self.GIFS)
                    
                } catch {
                    completion(result: false, Gifs: nil)
                    print("error parsing json")
                }
            }
            
        }.resume()
    
        }
    }
    
    

    
    
}