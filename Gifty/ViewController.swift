//
//  ViewController.swift
//  Gifty
//
//  Created by Halimjon Juraev on 6/29/16.
//  Copyright © 2016 Halimjon Juraev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, SelectedControl {

    @IBOutlet weak var SearchResultView: UIView!
    let network = Network()
    var GIFS = [Gifs]()

    
    @IBOutlet weak var Segment: ADVSegmentedControl!
    @IBOutlet weak var search: UISearchBar!
    
    @IBOutlet weak var collection: UICollectionView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        SearchResultView.hidden = true
        trending()
        collection.delegate = self
        collection.dataSource = self
        search.delegate = self
        Segment.delegate = self
        self.title = "Trending Gifs"
        search.showsCancelButton = true
    }
    
    func newSelection() {
    
        collection.reloadData()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        trending()
        searchBar.resignFirstResponder()
        searchBar.text = ""
        
    }

    func trending() {
        
        network.fetch("trending?", limit: "15", rating: "pg", format: "json") { (result, Gifs) in
            if result && Gifs != nil {
                dispatch_async(dispatch_get_main_queue()){
                self.GIFS = Gifs!
                self.collection.reloadData()
                }
            }
        }
        
    }
    
    func RatingControl() -> [Gifs] {
        var gifs = [Gifs]()
        switch Segment.selectedIndex {
        case 0:
            gifs = GIFS
            break
        case 1:
            for gif in GIFS {
                if gif.rate == "pg" {
                    gifs.append(gif)
                }
            }
            break
        case 2:
            for gif in GIFS {
                if gif.rate == "g" {
                    gifs.append(gif)
                }
            }
            break
        case 3:
            for gif in GIFS {
                if gif.rate == "y" {
                    gifs.append(gif)
                }
            }
            break
        default:
            gifs = GIFS
            break
        }
        return gifs
    }

    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        print("Search Clicked")
        if let text = searchBar.text {
            self.title = text
            search(text.stringByReplacingOccurrencesOfString(" ", withString: "+"))
        }
    }
    
    
    func search(keywords: String) {
    
        network.fetch("search?q=\(keywords)&", limit: "30", rating: "pg", format: "json") { (result, Gifs) in
            if result && Gifs != nil {
                dispatch_async(dispatch_get_main_queue()){
                    if Gifs?.isEmpty == false {
                        self.SearchResultView.hidden = true
                        self.GIFS = Gifs!
                        self.collection.reloadData()
                        self.search.resignFirstResponder()

                    } else {
                        self.GIFS = Gifs!
                        self.collection.reloadData()
                        self.SearchResultView.hidden = false
                    }

                }
            }
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (RatingControl()).count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let currentGif = (RatingControl())[indexPath.row]
        print(currentGif.rate)
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GifView", forIndexPath: indexPath) as? CollectionCell {
           
                cell.configureCell(currentGif)

            
            return cell
            
        } else {
            return UICollectionViewCell()
        }
        
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    }

}

